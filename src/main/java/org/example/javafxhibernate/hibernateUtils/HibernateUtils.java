package org.example.javafxhibernate.hibernateUtils;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
    private static final SessionFactory facSession;

    static {
        try{
            facSession = new Configuration().configure().buildSessionFactory();
        }catch (Throwable t){
            System.out.println(t.getMessage());
            System.out.println(t.getStackTrace());
            throw new ExceptionInInitializerError();
        }
    }

    public static SessionFactory getFacSession(){
        return facSession;
    }
}
