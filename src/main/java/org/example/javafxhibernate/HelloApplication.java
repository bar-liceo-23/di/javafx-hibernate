package org.example.javafxhibernate;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;
import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        File fHibernateConf = new File(HelloApplication.class.getResource("hibernate.cfg.xml").getFile());
        SessionFactory sessionFactory = new Configuration().configure(fHibernateConf).buildSessionFactory();
        Session session = sessionFactory.openSession();

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 500, 500);
        HelloController controller = fxmlLoader.getController();
        controller.setSession(session);
        stage.setTitle("Hello Hibernate!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}