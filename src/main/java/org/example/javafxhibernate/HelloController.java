package org.example.javafxhibernate;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class HelloController {
    @FXML
    private Label dataText;
    private Session session;

    public void setSession(Session session) {
        this.session = session;
    }

    @FXML
    protected void onReadDataButtonClick() {
        String separator = "\n";

        Query query = session.createQuery("FROM Test");

        List<Test> tests = query.list();
        tests.forEach(t -> System.out.println(t.getAtributo()));
        System.out.println("Reading student records...");
        String newText = new String();

        for (Test t : tests) {
            newText += t.getAtributo();
            newText += separator;
        }


        dataText.setText(newText);
    }
}