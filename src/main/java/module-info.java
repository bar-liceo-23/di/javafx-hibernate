module org.example.javafxhibernate {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.hibernate.orm.core;
    requires jakarta.persistence;
    requires java.sql;
    requires java.naming;


    opens org.example.javafxhibernate to javafx.fxml, org.hibernate.orm.core;
    exports org.example.javafxhibernate;

    //opens org.example.javafxhibernate;
}